#/bin/bash
yum -y install epel-release

echo "gem: --no-rdoc --no-ri --no-document" > ~/.gemrc

yum -y install ruby ruby-devel libxml2 libxslt zlib-devel nodejs
yum -y group install "Development tools"

gem install rails

yum -y install mysql-devel sqlite-devel
gem install sqlite3 mysql2 capistrano

yum -y install puppet
yum -y install git

gem update --system

cd /usr/src/
rails new demoapp
cd /usr/src/demoapp/
gem pristine --all
#Test demoapp
#bin/rails server -b 0.0.0.0

gem install r10k

mkdir -p /etc/puppetlabs/r10k
cat << EOF > /etc/puppetlabs/r10k/r10k.yaml
cachedir: /var/cache/r10k
sources:
 puppet:
  remote: 'https://jotacepea@bitbucket.org/jotacepea/r10k-rb.git'
  basedir: /etc/puppet/environments
EOF

cat << EOF > /usr/local/sbin/run-puppet
#!/bin/bash
r10k deploy environment -p
puppet apply /etc/puppet/environments/production/manifests/site.pp
EOF

chmod +x /usr/local/sbin/run-puppet
rm -rf /etc/puppet
git clone https://jotacepea@bitbucket.org/jotacepea/config-puppet.git /etc/puppet
mkdir /etc/puppet/environments
/usr/local/bin/r10k deploy environment -p
grep "include rbapp" /etc/puppet/environments/production/manifests/site.pp || echo "include rbapp" >> /etc/puppet/environments/production/manifests/site.pp

/usr/local/sbin/run-puppet
/usr/local/sbin/run-puppet
/usr/local/sbin/run-puppet

